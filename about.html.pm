#lang pollen

◊(define-meta title "About Dangermuse")

◊emph{Dangermuse} is a blog about role-playing games and other things
that strike my fancy.

This site is based on using Joel Dueck's
◊link["https://github.com/otherjoel/thenotepad"]{The Notepad} static
site generator, which is in turn built atop the almighty
◊link["//pollenpub.com"]{Pollen} by Matthew Butterick.

