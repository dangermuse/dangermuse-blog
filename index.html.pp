#lang pollen
◊(require racket/list pollen/pagetree pollen/template pollen/private/version)
<!DOCTYPE html>
<html lang="en" class="gridded">
    <head>
        <meta charset="utf-8">
        <meta name="generator" content="Racket ◊(version) + Pollen ◊|pollen:version|">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dangermuse</title>
        <link rel="stylesheet" href="/tufte.css">
        <link rel="stylesheet" href="/latex.css">
        <link rel="stylesheet" href="/layout.css">
    </head>
    <body>
        <header class="main">
            <p><a href="/" class="home">Dangermuse</a> <span class="tagline"></span></p>
            <nav>
                <ul>
                    <li class="current-section"><a href="/topics.html">Topics</a></li>
                    <li><a href="/about.html">About</a></li>
                    <li><a href="/feed.xml" class="rss" title="Subscribe to feed">Use RSS?</a></li>
                </ul>
            </nav>
        </header>
        
        ◊for/s[post (latest-posts 10)]{
           <article>
           ◊(hash-ref post 'header_html)
           ◊(hash-ref post 'html)
           </article>
           <hr>
        }

        <footer class="main">
            <ul>
                <li><a href="/feed.xml" class="rss" title="Subscribe to feed">RSS</a></li>
                <li><a href="mailto:comments@dangermuse.ca">comments@dangermuse.ca</a></li>
                <li>Source code <a href="https://codeberg.org/dangermuse/dangermuse-blog">on Codeberg</a></li>
                <li>Valid <a href="https://validator.w3.org/nu/?doc=https%3A%2F%2Fdangermuse.ca%2F">HTML5</a> + CSS</li>
                <li>Licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a></li>
            </ul>
        </footer>
    </body>
</html>
