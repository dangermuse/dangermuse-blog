#lang pollen

◊(require "util-topics.rkt"
           pollen/template
           pollen/pagetree
           pollen/private/version
           pollen/cache
           net/uri-codec)

◊(define main-pagetree (cached-doc (string->path "index.ptree")))

<!DOCTYPE html>
<html lang="en" class="gridded">
    <head>
        <meta charset="utf-8">
        <meta name="generator" content="Racket ◊(version) + Pollen ◊|pollen:version|">
        <title>Topics (Dangermuse)</title>
        <link rel="stylesheet" href="/tufte.css">
        <link rel="stylesheet" href="/latex.css">
        <link rel="stylesheet" href="/layout.css">
    </head>
    <body>
        <header class="main">
          <p><a href="/" class="home">Dangermuse</a> <span class="tagline"></span></p>
          <nav>
            <ul>
              <li><a href="/topics.html">Topics</a></li>
              <li><a href="/games.html">Cool stuff</a></li>
              <li><a href="/about.html">About</a></li>
              <li><a href="/feed.xml" class="rss" title="Subscribe to feed">Use RSS?</a></li>
            </ul>
          </nav>
        </header>
        <section class="main">
          <table>
            ◊for/s[topic (topic-list)]{
            <tr>
              <td><a id="◊(uri-encode (car topic))">◊(car topic)</a></td>
              <td><ul>
                  ◊for/s[post (cdr topic)]{
                  <li><a href="/◊(list-ref post 0)">◊(list-ref post 1)</a></li>
                  }</ul></td>
            </tr>
            }
          </table>
        </section>
        <footer class="main">
          <ul>
            <li><a href="/feed.xml" class="rss" title="Subscribe to feed">RSS</a></li>
            <li><a href="mailto:comments@dangermuse.ca">comments@dangermuse.ca</a></li>
            <li>Source code <a href="https://codeberg.org/dangermuse/dangermuse-blog">on Codeberg</a></li>
            <li>Valid <a href="https://validator.w3.org/nu/?doc=https%3A%2F%2Fdangermuse.ca%2F">HTML5</a> + CSS</li>
            <li>Licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a></li>
          </ul>
        </footer>
    </body>
</html>
